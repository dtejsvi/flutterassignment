import 'package:assignment/screens/call_detail_screen.dart';
import 'package:assignment/screens/create_account_screen.dart';
import 'package:assignment/screens/home_screen.dart';
import 'package:assignment/screens/personal_info_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
      routes: {
          CreateAccountScreen.routeName: (ctx) => CreateAccountScreen(),
          PersonalInfoScreen.routeName: (ctx) => PersonalInfoScreen(),
          CallDetailScreen.routeName: (ctx) => CallDetailScreen(),
          
        },
    );
  }
}


