import 'package:assignment/screens/personal_info_screen.dart';
import 'package:assignment/utils/common_utils.dart';
import 'package:assignment/widgets/status_widget.dart';
import 'package:flutter/material.dart';

class CreateAccountScreen extends StatefulWidget {
  CreateAccountScreen({Key key}) : super(key: key);
  static const routeName = "/create_account";
  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  TextEditingController textController = TextEditingController();
  bool isLowercase = false;
  bool isUppercase = false;
  bool isNumber = false;
  bool isvalideLength= false;
  @override
  void initState() {
    textController.addListener(() {
      setState(() {
        isLowercase = textController.text.contains(new RegExp(r'[a-z]'), 0);
        isUppercase = textController.text.contains(new RegExp(r'[A-Z]'), 0);
        isNumber = textController.text.contains(RegExp(r'\d'), 0);
        isvalideLength = textController.text.length > 9;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Utils.mainAppColor,
      appBar: AppBar(
        title: Text("Create Account"),
        backgroundColor: Utils.mainAppColor,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: StatusWidget(1),
              // child: Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: <Widget>[
              //     CircleAvatar(
              //       backgroundColor: Colors.black,
              //       radius: 23,
              //       child: CircleAvatar(
              //         backgroundColor: Colors.white,
              //         radius: 21,
              //         child: Center(
              //           child: Text(
              //             "1",
              //             style: TextStyle(color: Colors.black),
              //           ),
              //         ),
              //       ),
              //     ),
              //     Container(
              //         height: 50,
              //         width: 30,
              //         child: Divider(
              //           height: 10,
              //           color: Colors.black,
              //           thickness: 2,
              //         )),
              //     CircleAvatar(
              //       backgroundColor: Colors.black,
              //       radius: 23,
              //       child: CircleAvatar(
              //         backgroundColor: Colors.white,
              //         radius: 21,
              //         child: Center(
              //           child: Text(
              //             "2",
              //             style: TextStyle(color: Colors.black),
              //           ),
              //         ),
              //       ),
              //     ),
              //     Container(
              //         height: 50,
              //         width: 30,
              //         child: Divider(
              //           height: 10,
              //           color: Colors.black,
              //           thickness: 2,
              //         )),
              //     CircleAvatar(
              //       backgroundColor: Colors.black,
              //       radius: 23,
              //       child: CircleAvatar(
              //         backgroundColor: Colors.white,
              //         radius: 21,
              //         child: Center(
              //           child: Text(
              //             "3",
              //             style: TextStyle(color: Colors.black),
              //           ),
              //         ),
              //       ),
              //     ),
              //     Container(
              //         height: 50,
              //         width: 30,
              //         child: Divider(
              //           height: 10,
              //           color: Colors.black,
              //           thickness: 2,
              //         )),
              //     CircleAvatar(
              //       backgroundColor: Colors.black,
              //       radius: 23,
              //       child: CircleAvatar(
              //         backgroundColor: Colors.white,
              //         radius: 21,
              //         child: Center(
              //           child: Text(
              //             "4",
              //             style: TextStyle(color: Colors.black),
              //           ),
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Create Password",
                    style: TextStyle(
                        color: Utils.buttonTextColor,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Password will be used to login to account",
                style: TextStyle(
                  color: Utils.buttonTextColor,
                  fontSize: 13,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 17, bottom: 20),
              child: TextFormField(
                controller: textController,
                obscureText: true,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    filled: true,
                    fillColor: Colors.grey.shade200,
                    hintText: 'Create Password',
                    suffixIcon: Icon(Icons.visibility)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Complexity:",
                  style: TextStyle(
                    color: Utils.buttonTextColor,
                    fontSize: 13,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      isLowercase
                          ? Icon(Icons.check_circle,color: Colors.green,)
                          : Text(
                              "a",
                              style: TextStyle(
                                  fontSize: 25, color: Utils.buttonTextColor),
                            ),
                      Text("Lowercase",
                          style: TextStyle(
                              fontSize: 13, color: Utils.buttonTextColor))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      isUppercase? Icon(Icons.check_circle,color: Colors.green,):
                      Text(
                        "A",
                        style: TextStyle(
                            fontSize: 25, color: Utils.buttonTextColor),
                      ),
                      Text("Uppercase",
                          style: TextStyle(
                              fontSize: 13, color: Utils.buttonTextColor))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      isNumber? Icon(Icons.check_circle,color: Colors.green,):
                      Text(
                        "123",
                        style: TextStyle(
                            fontSize: 25, color: Utils.buttonTextColor),
                      ),
                      Text("Number",
                          style: TextStyle(
                              fontSize: 13, color: Utils.buttonTextColor))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      isvalideLength? Icon(Icons.check_circle,color: Colors.green,):
                      Text(
                        "9+",
                        style: TextStyle(
                            fontSize: 25, color: Utils.buttonTextColor),
                      ),
                      Text("Characters",
                          style: TextStyle(
                              fontSize: 13, color: Utils.buttonTextColor))
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ButtonTheme(
                  minWidth: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pushNamed(
                          context, PersonalInfoScreen.routeName);
                    },
                    child: const Text('Next', style: TextStyle(fontSize: 20)),
                    color: Utils.buttonColor,
                    textColor: Colors.white,
                    elevation: 5,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
