import 'package:assignment/screens/create_account_screen.dart';
import 'package:assignment/utils/common_utils.dart';
import 'package:assignment/widgets/curve_painter.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TextEditingController textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/curve_image_1.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: null,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Welcome to",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                new RichText(
                  text: new TextSpan(
                    style: new TextStyle(
                        fontSize: 30.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      new TextSpan(text: 'GIN'),
                      new TextSpan(
                          text: ' Finans',
                          style: new TextStyle(color: Colors.blue)),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text(
                    "Welcome to The Bank of The Future.\nManage and track your accounts on \nthe go",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),

                    //#0080ff
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller: textController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            filled: true,
                            border: InputBorder.none,
                            fillColor: Colors.grey.shade200,
                            hintText: 'Email',
                            prefixIcon: Icon(Icons.email)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: Padding(
              padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ButtonTheme(
                  minWidth: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      if (textController.text != null &&
                          textController.text.isNotEmpty) {
                        Navigator.pushNamed(
                            context, CreateAccountScreen.routeName);
                      } else {
                        SnackBar(
                          content: Text(
                            'Please enter valid email id',
                            style:
                                TextStyle(fontSize: 16, fontFamily: 'segoeui'),
                          ),
                          duration: Duration(seconds: 5),
                        );
                      }
                    },
                    child: const Text('Next', style: TextStyle(fontSize: 20)),
                    color: Utils.buttonColor,
                    textColor: Colors.white,
                    elevation: 5,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    )
        // Container(
        //   child:

        //   Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: <Widget>[
        //       Container(
        //         width: double.infinity,
        //         height: 200,
        //         decoration: BoxDecoration(
        //           image: DecorationImage(
        //             image: AssetImage("assets/images/curve_image_1.png"),
        //             fit: BoxFit.cover,
        //           ),
        //         ),
        //         child: null,
        //       ),
        //       Padding(
        //         padding: const EdgeInsets.only(left: 20, right: 10),
        //         child: Column(
        //           crossAxisAlignment: CrossAxisAlignment.start,
        //           children: <Widget>[
        //             Text(
        //               "Welcome to",
        //               style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        //             ),
        //             new RichText(
        //               text: new TextSpan(
        //                 style: new TextStyle(
        //                     fontSize: 30.0,
        //                     color: Colors.black,
        //                     fontWeight: FontWeight.bold),
        //                 children: <TextSpan>[
        //                   new TextSpan(text: 'GIN'),
        //                   new TextSpan(
        //                       text: ' Finans',
        //                       style: new TextStyle(color: Colors.blue)),
        //                 ],
        //               ),
        //             ),
        //             Padding(
        //               padding: const EdgeInsets.only(top: 10),
        //               child: Text(
        //                 "Welcome to The Bank of The Future.\nManage and track your accounts on \nthe go",
        //                 style:
        //                     TextStyle(fontSize: 15, fontWeight: FontWeight.bold),

        //                 //#0080ff
        //               ),
        //             ),
        //             Padding(
        //               padding: const EdgeInsets.only(top: 30),
        //               child: Container(
        //                 decoration: BoxDecoration(
        //                     shape: BoxShape.rectangle,
        //                     color: Colors.white,
        //                     borderRadius: BorderRadius.all(Radius.circular(5))),
        //                 child: Padding(
        //                   padding: const EdgeInsets.all(8.0),
        //                   child: TextFormField(
        //                     controller: textController,
        //                     keyboardType: TextInputType.emailAddress,
        //                     decoration: InputDecoration(
        //                         filled: true,
        //                         border: InputBorder.none,
        //                         fillColor: Colors.grey.shade200,
        //                         hintText: 'Email',
        //                         prefixIcon: Icon(Icons.email)),
        //                   ),
        //                 ),
        //               ),
        //             ),
        //           ],
        //         ),
        //       ),
        //       Expanded(
        //         child: Padding(
        //           padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
        //           child: Align(
        //             alignment: Alignment.bottomCenter,
        //             child: ButtonTheme(
        //               minWidth: double.infinity,
        //               child: RaisedButton(
        //                 onPressed: () {
        //                   if (textController.text != null &&
        //                       textController.text.isNotEmpty) {
        //                     Navigator.pushNamed(
        //                         context, CreateAccountScreen.routeName);
        //                   } else {
        //                     SnackBar(
        //                       content: Text(
        //                         'Please enter valid email id',
        //                         style: TextStyle(
        //                             fontSize: 16, fontFamily: 'segoeui'),
        //                       ),
        //                       duration: Duration(seconds: 5),
        //                     );
        //                   }
        //                 },
        //                 child: const Text('Next', style: TextStyle(fontSize: 20)),
        //                 color: Utils.buttonColor,
        //                 textColor: Colors.white,
        //                 elevation: 5,
        //               ),
        //             ),
        //           ),
        //         ),
        //       )
        //     ],
        //   ),

        // ),
        );
  }
}
