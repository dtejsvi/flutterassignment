import 'package:assignment/utils/common_utils.dart';
import 'package:assignment/widgets/status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CallDetailScreen extends StatefulWidget {
  static const routeName = "/call_detail";
  CallDetailScreen({Key key}) : super(key: key);

  @override
  _CallDetailScreenState createState() => _CallDetailScreenState();
}

class _CallDetailScreenState extends State<CallDetailScreen>
    with SingleTickerProviderStateMixin {
  List<String> _leaveTypes = [
    'Earned Leave',
    'Unpaid Leave',
    'Optional Holiday',
    'Comp Off',
    'Work From Home',
    'On Duty',
    'Apply All',
  ];
  int _selectedIndex = 1;
  Animation<double> animation;
  AnimationController _controller;

  DateTime newDateValue;
  TextEditingController dateTextController = TextEditingController();
  TextEditingController timeTextController = TextEditingController();
  var dateText = "- Choose Date -";
  var timeText = "- Choose Time -";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller =
        new AnimationController(vsync: this, duration: Duration(seconds: 3))
          ..repeat();
    animation = new CurvedAnimation(parent: _controller, curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      backgroundColor: Utils.mainAppColor,
      appBar: AppBar(
        title: Text("Create Account"),
        backgroundColor: Utils.mainAppColor,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: StatusWidget(3),
          ),
          Align(
            alignment: Alignment.centerLeft,
                      child: new Container(
              child:
                  // new Center(
                  //   child:
                  new ScaleTransition(
                scale: animation,
                child: CircleAvatar(
                    backgroundColor: Utils.buttonColor,
                    radius: 30,
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 20,
                      child: Icon(Icons.calendar_today),
                    )),
              ),
              //),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30, bottom: 10),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Schedule Video Call",
                  style: TextStyle(
                      color: Utils.buttonTextColor,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Choose the date ans time that you preferred, we will send a link via email to make a video call on the scheduled date and time",
              style: TextStyle(
                color: Utils.buttonTextColor,
                fontSize: 13,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              showDateCalendar(context).then(
                (selectedDate) {
                  DateTime dateTime = selectedDate;
                  if (dateTime != null) {
                    setState(() {
                      dateText = new DateFormat("EEE, d MMM yyyy")
                          .format(dateTime)
                          .toString(); //dateTime.toString();
                    });
                  }
                },
              );
            },
            child: Container(
                margin: EdgeInsets.only(top: 30),
                height: 45,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(dateText, style: TextStyle(fontSize: 20)),
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                )),
          ),
          InkWell(
            onTap: () {
              showTimePickerPopup(context).then((onValue) {
                print("Time of day------${onValue.toString()}");
                String min;
                setState(() {
                  if (onValue.minute < 10) {
                    min = "0" + onValue.minute.toString();
                  } else {
                    min = onValue.minute.toString();
                  }
                  timeText = onValue.hour.toString() + ":" + min;
                });
              });
              // showDateCalendar(context).then(
              //   (selectedDate) {
              //     DateTime dateTime = selectedDate;
              //     if (dateTime != null) {
              //       setState(() {

              //       timeText = dateTime.toString();
              //       });

              //     }
              //   },
              // );
            },
            child: Container(
                margin: EdgeInsets.only(top: 20),
                height: 45,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(timeText, style: TextStyle(fontSize: 20)),
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                )),
          ),
        ]),
      ),
    ));
  }

  //time code

  Future<TimeOfDay> showTimePickerPopup(BuildContext context) async {
    return (Utils.isAndroidDevice())
        ? showAndroidTimePicker(context)
        : showIOSTimePicker(context);
  }

  Future<TimeOfDay> showAndroidTimePicker(BuildContext context) async {
    return await showTimePicker(initialTime: TimeOfDay.now(), context: context);
  }

  Duration initialtimer = new Duration();
  showIOSTimePicker(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            // shape: RoundedRectangleBorder(
            //     borderRadius:
            //         BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: MediaQuery.of(context).size.height - 300, // - 80,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //Time picker
                    CupertinoTimerPicker(
                      //backgroundColor: Colors.blue,
                      onTimerDurationChanged: (Duration changedtimer) {
                        initialtimer = changedtimer;
                        //print("Duration is: $changedtimer");
                      },
                      mode: CupertinoTimerPickerMode.hm,
                      minuteInterval: 5,
                      secondInterval: 5,
                      initialTimerDuration: initialtimer,
                    ),

                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Utils.mainAppColor,
                          ),
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        Expanded(
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            onPressed: () {
                              closeTimePickerDialog(context, initialtimer);
                            },
                            child: Text(
                              "Save",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Utils.mainAppColor,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void closeTimePickerDialog(BuildContext context, Duration initialtimer) {
    Navigator.pop(context);
    initialtimer.inDays;
    //durationController.text = "";
    List<String> timeList = initialtimer.toString().split(":");
    //durationController.text = timeList[0];

    setState(() {
      timeText = timeList[0] + ":" + timeList[1];
    });
  }

//Calendar code

  Future<DateTime> showDateCalendar(BuildContext context) async {
    return (Utils.isAndroidDevice())
        ? showAndroidCalendar(context)
        : showIOSCalendar(context);
  }

  Future<DateTime> showAndroidCalendar(
    BuildContext context,
  ) {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2022),
    ).then((pickedValue) {
      return pickedValue;
    });
  }

  Future<DateTime> showIOSCalendar(
    BuildContext context,
  ) {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
              height: MediaQuery.of(context).copyWith().size.height / 2 - 100,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 10, top: 5),
                        height: 40,
                        child: Text(
                          "Select Date",
                          style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'segoeui',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      FlatButton.icon(
                        icon: Icon(Icons.done),
                        label: Text(
                          'Done',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(newDateValue);
                        },
                      )
                    ],
                  ),
                  Expanded(
                    child: Opacity(
                      opacity: 1.0,
                      child: showDatePickerForiOS(),
                    ),
                  )
                ],
              ));
        });
  }

  Widget showDatePickerForiOS() {
    return CupertinoDatePicker(
      initialDateTime: DateTime(2020),
      onDateTimeChanged: (DateTime newdate) {
        newDateValue = newdate;
      },
      use24hFormat: true,
      minimumYear: 2020,
      maximumYear: 2020,
      minuteInterval: 1,
      mode: CupertinoDatePickerMode.dateAndTime,
    );
  }
}
