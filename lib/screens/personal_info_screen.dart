import 'package:assignment/screens/call_detail_screen.dart';
import 'package:assignment/utils/common_utils.dart';
import 'package:assignment/widgets/status_widget.dart';
import 'package:flutter/material.dart';

class PersonalInfoScreen extends StatefulWidget {
  PersonalInfoScreen({Key key}) : super(key: key);
  static const routeName = "/personal_info";

  @override
  _PersonalInfoScreenState createState() => _PersonalInfoScreenState();
}

class _PersonalInfoScreenState extends State<PersonalInfoScreen> {
  List<String> _goalTypes = [
    'Vehicle purchase',
    'Home purchase',
    'Higher education',
    'Land purchase',
  ];

  List<String> _incomeType = [
    '<50000',
    '50000-100000',
    '100000-500000',
    '500000-800000',
    '800000-1000000',
    '>1000000',
  ];

  List<String> _expenseType = [
    'Daily living',
    'Vehicle maintenance',
    'Medical expense',
    'House rent',
    'EMI',
  ];
  int _selectedIndex = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      backgroundColor: Utils.mainAppColor,
      appBar: AppBar(
        title: Text("Create Account"),
        backgroundColor: Utils.mainAppColor,
        elevation: 0,
      ),
      body: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: StatusWidget(2),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30, bottom: 10),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Personal Information",
                    style: TextStyle(
                        color: Utils.buttonTextColor,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Please fill in the information below and your goal for digital saving",
                style: TextStyle(
                  color: Utils.buttonTextColor,
                  fontSize: 13,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 14,
                      ),
                      child: Text(
                        "Goal for activation",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                      ),
                    ),
                    DropdownButton<String>(
                      iconDisabledColor: Utils.buttonColor,
                      isExpanded: true,
                      focusColor: Colors.grey,
                      underline: Divider(
                        height: 0,
                        color: Colors.transparent,
                      ),
                      hint: Padding(
                        padding: EdgeInsets.only(left: 14),
                        child: Text(
                          _goalTypes[_selectedIndex],
                          style: TextStyle(
                            color: Utils.buttonColor,
                          ),
                        ),
                      ),
                      onChanged: (newValue) {
                        setState(() {
                          _selectedIndex = _goalTypes.indexOf(newValue);
                        });
                      },
                      items: _goalTypes.map((leave) {
                        return new DropdownMenuItem(
                          child: new Text(leave),
                          value: leave,
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 14,
                      ),
                      child: Text(
                        "Monthly income",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                      ),
                    ),
                    DropdownButton<String>(
                      iconDisabledColor: Utils.buttonColor,
                      isExpanded: true,
                      focusColor: Colors.grey,
                      underline: Divider(
                        height: 0,
                        color: Colors.transparent,
                      ),
                      hint: Padding(
                        padding: EdgeInsets.only(left: 14),
                        child: Text(
                          _incomeType[_selectedIndex],
                          style: TextStyle(
                            color: Utils.buttonColor,
                          ),
                        ),
                      ),
                      onChanged: (newValue) {
                        setState(() {
                          _selectedIndex = _incomeType.indexOf(newValue);
                        });
                      },
                      items: _incomeType.map((leave) {
                        return new DropdownMenuItem(
                          child: new Text(leave),
                          value: leave,
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 14,
                      ),
                      child: Text(
                        "Monthly expense",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                      ),
                    ),
                    DropdownButton<String>(
                      iconDisabledColor: Utils.buttonColor,
                      isExpanded: true,
                      focusColor: Colors.grey,
                      underline: Divider(
                        height: 0,
                        color: Colors.transparent,
                      ),
                      hint: Padding(
                        padding: EdgeInsets.only(left: 14),
                        child: Text(
                          _expenseType[_selectedIndex],
                          style: TextStyle(
                            color: Utils.buttonColor,
                          ),
                        ),
                      ),
                      onChanged: (newValue) {
                        setState(() {
                          _selectedIndex = _expenseType.indexOf(newValue);
                        });
                      },
                      items: _expenseType.map((leave) {
                        return new DropdownMenuItem(
                          child: new Text(leave),
                          value: leave,
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ButtonTheme(
                  minWidth: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, CallDetailScreen.routeName);
                    },
                    child: const Text('Next', style: TextStyle(fontSize: 20)),
                    color: Utils.buttonColor,
                    textColor: Colors.white,
                    elevation: 5,
                  ),
                ),
              ),
            )
          ])),
    ));
  }
}
