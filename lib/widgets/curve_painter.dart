
import 'package:flutter/material.dart';

class CurvePainter extends StatefulWidget {
  CurvePainter({Key key}) : super(key: key);

  @override
  _CurvePainterState createState() => _CurvePainterState();
}

class _CurvePainterState extends State<CurvePainter> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: CustomPaint(painter: CurvePainter_(),
    ));
  }
}

class CurvePainter_ extends CustomPainter
{
  @override
  void paint(Canvas canvas, Size size) {
    print("I am painting my screen");
     var paint = Paint();
    paint.color = Colors.green[800];
    paint.style = PaintingStyle.fill;

    var path = Path();

    // path.moveTo(0, size.height * 0.9167);
    // path.quadraticBezierTo(size.width * 0.25, size.height * 0.875,
    //     size.width * 0.5, size.height * 0.9167);
    // path.quadraticBezierTo(size.width * 0.75, size.height * 0.9584,
    //     size.width * 1.0, size.height * 0.9167);
    // path.lineTo(size.width, size.height);
    // path.lineTo(0, size.height);

    path.moveTo(0, size.height * 0.25);
    // path.quadraticBezierTo(
    //     70, 5, size.width, size.height * 0.32, );
    path.quadraticBezierTo(
        size.width / 5, size.height / 20, size.width, size.height * 0.32, );
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }

}