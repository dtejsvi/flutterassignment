import 'package:flutter/material.dart';

class StatusWidget extends StatefulWidget {
  int highlighedSection;
  StatusWidget(this.highlighedSection);

  @override
  _StatusWidgetState createState() => _StatusWidgetState();
}

class _StatusWidgetState extends State<StatusWidget> {

Widget buildCircle(int stepNumber)
{
  return CircleAvatar(
                    backgroundColor: Colors.black,
                    radius: 23,
                    child: CircleAvatar(
                      backgroundColor: //widget.highlighedSection == stepNumber || 
                      widget.highlighedSection >= stepNumber? Colors.green:Colors.white,
                      radius: 21,
                      child: Center(
                        child: Text(
                          stepNumber.toString(),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ),
                  );
}

  @override
  Widget build(BuildContext context) {
    return Container(
       child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  buildCircle(1),
                  // CircleAvatar(
                  //   backgroundColor: Colors.black,
                  //   radius: 23,
                  //   child: CircleAvatar(
                  //     backgroundColor: Colors.white,
                  //     radius: 21,
                  //     child: Center(
                  //       child: Text(
                  //         "1",
                  //         style: TextStyle(color: Colors.black),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  Container(
                      height: 50,
                      width: 30,
                      child: Divider(
                        height: 10,
                        color: Colors.black,
                        thickness: 2,
                      )),
                  buildCircle(2),
                  Container(
                      height: 50,
                      width: 30,
                      child: Divider(
                        height: 10,
                        color: Colors.black,
                        thickness: 2,
                      )),
                  buildCircle(3),
                  Container(
                      height: 50,
                      width: 30,
                      child: Divider(
                        height: 10,
                        color: Colors.black,
                        thickness: 2,
                      )),
                  buildCircle(4),
                ],
              ),
    );
  }
}