
import 'dart:io';

import 'package:assignment/utils/colorCodes.dart';
import 'package:flutter/material.dart';

class Utils {

static Color mainAppColor = AppColorCodes("#2460ff");   //#0080ff
static Color buttonColor = AppColorCodes("#6cb6ff");
static Color buttonTextColor = AppColorCodes("#ffffff");

  static bool isAndroidDevice() {
    return Platform.isAndroid;
  }

  static bool isIOSDevice() {
    return Platform.isIOS;
  }

}